Mercado Libre es una aplicación muy conocida para permitir la compra/venta en línea de todo tipo de artículos
 en forma directa por cualquier usuario. Se solicita simular los resultados de una búsqueda en Mercado Libre de un
  artículo x. Para ello, se requiere la realización de dos módulos independientes (registro y principal).

Cargar automáticamente los resultados de la búsqueda en un arreglo de n registros (n se ingresa por teclado). Por cada registro se debe indicar:

código de publicación
precio
ubicación geográfica (1-23 identificando cada provincia de la Argentina)
estado (nuevo/usado)
cantidad disponible
puntuación del vendedor ( 1-5 identificando 5 como Excelente y 1 Mala).
El arreglo debe quedar ordenado por código de publicación. Mostrar el vector generado.

Presentar un menú de opciones que permita realizar lo siguiente:

1)    Nuevos por precio: generar un nuevo arreglo conteniendo sólo las publicaciones de artículos nuevos. Mostrar el listado ordenado por precio (de menor a mayor).

2)    Usados por calificación: determinar cantidad de publicaciones en estado usado por cada puntuación de vendedor (5 totales).

3)    Distribución geográfica: informar la cantidad de artículos disponibles por ubicación geográfica y puntuación del vendedor, pero mostrando el resultado en forma de matriz. Se debe mostrar el nombre de cada provincia (no el código) y la descripción de la puntuación del vendedor (no el código), y solo para las cantidades mayores a cero.

4)    Total provincial: a partir de la matriz, informar el total de artículos para una provincia que se ingresa por teclado (validar que se encuentre cargada).

5)    Precio promedio de usados: mostrar las publicaciones con estado usado, cuyo precio supera el precio promedio de usados del listado.

6)    Compra ideal: informar cual es el menor precio para un artículo nuevo, omitiendo los vendedores con calificación Mala.

7)    Comprar: buscar una publicación cuyo código se ingresa por teclado. Si no existe, informar con un mensaje. Si existe, preguntar al usuario qué cantidad de artículos desea comprar, validar que la cantidad disponible sea suficiente, y confirmar/rechazar la compra según corresponda.
