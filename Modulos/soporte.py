from random import *

# Variables Globales.
ESTADO = ("Nuevo", "Usado")
PUNTUACION = ("1.Mala", "2.Regular", "3.Buena", "4.Muy Buena", "5.Excelente")
PROVINCIAS = ('Buenos Aires', 'Catamarca', 'Chaco', 'Chubut', 'Cordoba', 'Corrientes', 'Entre Rios',
              'Formosa', 'Jujuy', 'La Pampa', 'La Rioja', 'Mendoza', 'Misiones', 'Neuquen',
              'Rio Negro', 'Salta', 'San Juan', 'San Luis', 'Santa Cruz', 'Santa Fe',
              'Santiago del Estero', 'Tierra del Fuego', 'Tucuman')


# Definicion de la Clase Articulo.
class Articulo:
    def __init__(self, codigo, precio, ubicacion, estado, cantidad, puntuacion):
        self.codigo = codigo
        self.precio = precio
        self.ubicacion = ubicacion
        self.estado = estado
        self.cantidad = cantidad
        self.puntuacion = puntuacion


# Funcion que permite crear y cargar de manera automatica el vector con los articulos.
def carga(n):
    v = [None] * n
    for i in range(n):
        v[i] = Articulo(randint(1, 9999), randint(100, 5000), randint(1, 23),
                        randint(1, 2), randint(0, 50), randint(1, 5))
    return v


# To string nos permite darle formato a cada articulo en una sola linea horizontal
def to_string(articulo):
    r = ''
    r += '{:<15}'.format('Codigo: ' + str(articulo.codigo))
    r += '{:<16}'.format('Precio: $' + str(articulo.precio))
    r += '{:<36}'.format('Ubicacion: ' + PROVINCIAS[articulo.ubicacion - 1])
    r += '{:<18}'.format('Estado: ' + ESTADO[articulo.estado - 1])
    r += '{:<26}'.format('Cantidad disponible: ' + str(articulo.cantidad))
    r += '{:<20}'.format('Puntuacion del vendedor: ' + PUNTUACION[articulo.puntuacion - 1])
    return r


# Ordenamiento de Shell con modificacion que permite optar que ordenar
def shell_sort(v, opcion):
    n = len(v)
    h = 1
    while h <= n // 9:
        h = 3 * h + 1
    while h > 0:
        for j in range(h, n):
            y = v[j]
            k = j - h
            if opcion == 1:
                while k >= 0 and y.codigo < v[k].codigo:
                    v[k + h] = v[k]
                    k -= h
            else:
                while k >= 0 and y.precio < v[k].precio:
                    v[k + h] = v[k]
                    k -= h
            v[k + h] = y
        h //= 3


# Permite hacer un print de un vector con el formato dado por la funcion to_string.
def mostrar_todo(v):
    for i in range(len(v)):
        print(to_string(v[i]))


# Validador que evita que corra la opcion 4 sin antes haber cargado la matriz en la opcion 3.
def validar_op4(vector):
    if len(vector) == 0:
        print('No hay datos cargados para poder validar la busqueda. Por favor, ingrese a la opcion 3 primero.')
        return False
    else:
        return True


# Validador generico de numeros positivos
def validar_dato():
    x = int(input("Ingrese un numero:"))
    while x <= 0:
        print("Error! Debe ingresar un numero mayor a cero")
        x = int(input("Ingrese un numero:"))
    return x


# Validador para el menu, restringe la eleccion a la cantidad de opciones.
def validar_menu():
    x = int(input("Su opcion:"))
    while x < 1 or x > 8:
        print("Error!, Debe ingresar un numero entre 1 y 8")
        x = int(input("Su opcion:"))
    return x


def test():
    pass


if __name__ == '__main__':
    test()
