from Modulos.soporte import *


# Nuevos por precio: generar un nuevo arreglo conteniendo sólo las publicaciones de artículos nuevos.
def opcion_1(vector):
    n = len(vector)
    # Creacion de un vector vacio que contendrá los articulos nuevos.
    art_nuevo = []

    # Recorrer el vector en busca de articulos nuevos.
    for i in range(n):
        if vector[i].estado == 1:
            art_nuevo.append(vector[i])

    # Ordenamiento del vector.
    shell_sort(art_nuevo, 2)

    # Mostrar el listado del vector.
    print("\033[4;m" + "\nListado de articulos nuevos, ordenados de menor a mayor por precio:" + '\033[;m')
    mostrar_todo(art_nuevo)


# Opcion 2: usados por clasificacion. Ciclo For recorre el vector original, se realiza busqueda
def opcion_2(vector):
    usados = [0] * 5
    for i in range(len(vector)):
        if vector[i].estado == 2:
            usados[vector[i].puntuacion - 1] += 1

    print("\033[4;m" + '\nCantidad de Articulos en Estado Usado por Clasificacion del Vendedor: ' + '\033[;m')

    for i in range(5):
        print('Clasificacion', (5-i), 'Estrellas - Cantidad de articulos:', usados[4-i])


# Distribución geográfica: informar la cantidad de artículos disponibles por ubicación geográfica y puntuación del vendedor.
def opcion_3(v_art):

    n = len(v_art)

    # Crear matriz 23*6.
    matriz = [None] * 23
    for i in range(23):
        matriz[i] = [0] * 6
        # Cargar en la columna [0] las provincias.
        matriz[i][0] = PROVINCIAS[i]

    # Cargar los datos en la matriz.
    for i in range(n):
        if v_art[i].cantidad > 0:
            matriz[v_art[i].ubicacion-1][v_art[i].puntuacion] += 1

    # Creacion de la estructura basica de la tabla.
    # Las llaves "{}" indican donde aplica la funcion .format
    tablaraiz = "+--------------------------------------------------------------+\n" \
                "| Ubicacion/Puntuacion |Mala |Regular|Buena|Muy Buena|Excelente|\n" \
                "|----------------------|-----|-------|-----|---------|---------|\n" \
                "{}" \
                "+--------------------------------------------------------------+"

    # Dandole formato a la matriz con .format
    tabla = ''
    for i in range(23):
        tabla += "|{:<22}|{:^5}|{:^7}|{:^5}|{:^9}|{:^9}|\n".format(*matriz[i])

    # Imprimiendo la tabla.
    print("\033[4;m" + "\nDistribucion Geografica:" + '\033[;m')
    print(tablaraiz.format(tabla))

    return matriz


# Total provincial: a partir de la matriz, informar el total de artículos para una provincia que se ingresa por teclado.
def opcion_4(matriz):
    cont_art = 0
    bandera = True
    print("\033[4;m" + '\nTotal de articulos por provincia:' + '\033[;m')
    while bandera:
        x = input('Ingrese la provincia:').lower()
        for i in range(23):
            if x == PROVINCIAS[i].lower():
                for j in range(1, 6):
                    cont_art += matriz[i][j]
                print('\nProvincia: ' + matriz[i][0] + ' - Cantidad de total de articulos: ' + str(cont_art))
                bandera = False
                break
        if bandera:
            print("No se ha encontrado la provincia vuelva a ingresar.")


# Precio promedio de usados: mostrar las publicaciones con estado usado, cuyo precio supera el precio promedio de usados del listado.
def opcion_5(vector):
    total_precio_usados = 0
    total_cant_usados = 0
    # Sumar los precios y la cantidad de articulos usados.
    for i in range(len(vector)):
        if vector[i].estado == 2:
            total_precio_usados += vector[i].precio
            total_cant_usados += 1

    # Se saca el promedio en base los datos obtenidos del primer FOR
    promedio_prec_usados = total_precio_usados // total_cant_usados
    print("\033[4;m" + '\nListado de articulos de usados que superan el promedio:' + '\033[;m' +
          ' $' + str(promedio_prec_usados))
    for j in range(len(vector)):
        if vector[j].estado == 2 and vector[j].precio > promedio_prec_usados:
            print(to_string(vector[j]))
    print()


# Compra ideal: informar cual es el menor precio para un artículo nuevo, omitiendo los vendedores con calificación Mala.
def opcion_6(v_art):
    n = len(v_art)
    menorprecio = None
    # Ordenar el vector de menor a mayor precio.
    v = v_art[:]
    shell_sort(v, 2)
    for i in range(n):
        if v[i].puntuacion > 1:
            menorprecio = v[i]
            break
    print("\033[4;m" + "\nArticulo con menor precio:" + '\033[;m')
    print(to_string(menorprecio))


# Comprar: buscar una publicación cuyo código se ingresa por teclado.
def opcion_7(vector):
    print("\033[4;m" + '\nBIENVENIDO AL SISTEMA DE COMPRA' + '\033[;m')
    n = len(vector)
    print('\nIngrese el codigo del articulo que esta buscando: ')
    buscar_cod = validar_dato()
    # Bandera que permite dar un mensaje si no se encontro el articulo buscado.
    b_encontrado = False
    for i in range(n):
        if buscar_cod == vector[i].codigo:
            b_encontrado = True
            print('\nSu articulo es:', to_string(vector[i]))
            print('\nQue cantidad de articulos desea comprar?: ')
            cant = validar_dato()
            while cant > vector[i].cantidad:
                print('\nCOMPRA RECHAZADA', '\nLa cantidad solicitada supera el STOCK del producto. Intente otra vez!')
                print('Que cantidad de articulos desea comprar?: ')
                cant = validar_dato()
            print('COMPRA EXITOSA! Disfrute su producto =D\n')
            break
    if not b_encontrado:
        print('El articulo N°' + str(buscar_cod) + ' , no existe en la base de datos.')


# Menu de opciones
def menu():
    print('\033[4;m' + "Bienvenido al KWIK-E-MART" + '\033[;m')
    print('Ingrese la cantidad de articulos que desea crear. Por favor, mayor que 0: ')
    n = validar_dato()
    print('\033[4;m' + '\nLista Completa de Articulos' + '\033[;m')
    # Carga del vector con los articulos
    lista_de_articulos = carga(n)
    # Ordenamiento del vector por codigo de articulo
    shell_sort(lista_de_articulos, 1)
    # Print en pantalla
    mostrar_todo(lista_de_articulos)
    # Vector que se utilizara en la opcion 4, en base la opcion 3
    matriz_op4 = []

    opc = 0
    while opc != 8:

        print('\033[4;m' + "\nMENU DE OPCIONES" + '\033[;m')
        print('1) Nuevos por precio')
        print('2) Usados por calificación')
        print('3) Distribución geográfica')
        print('4) Total provincial')
        print('5) Precio promedio de usados')
        print('6) Compra ideal')
        print('7) Comprar')
        print('8) Salir')

        opc = validar_menu()

        if opc == 1:
            opcion_1(lista_de_articulos)
        elif opc == 2:
            opcion_2(lista_de_articulos)
        elif opc == 3:
            matriz_op4 = opcion_3(lista_de_articulos)
        elif opc == 4:
            # Si el vector no fue cargado en la opcion 3, te devuelve al menu.
            if validar_op4(matriz_op4):
                opcion_4(matriz_op4)
        elif opc == 5:
            opcion_5(lista_de_articulos)
        elif opc == 6:
            opcion_6(lista_de_articulos)
        elif opc == 7:
            opcion_7(lista_de_articulos)
        elif opc == 8:
            print('---Gracias por su compra---')


if __name__ == '__main__':
    menu()
    print('Fin del programa.')
